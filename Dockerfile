# This Dockerfile is a bit like a Makefile, except
# that it contains the recipe to build a Docker Image
# (like a virtual machine)

# The parent or base image contains many but not all of the cosmosis
# dependencies environment variables.  See the Dockerfile in base-image
# for how that was created originally (it will be downloaded from 
# hub.docker.io).

FROM joezuntz/cosmosis:2.0



# USER-INSTALLED PACKAGES SHOULD GO AFTER HERE.

# If you need more libraries or other 
# tools you can put the commands to install them in here, by 
# prefixing the install commands with "RUN".
# Here are some examples:

# You can install with pip:
# pip install treecorr

# Or with the apt-get command from ubuntu. You can run the VM and search
# inside for what is available like this:
# ./start-cosmosis-vm
# apt-cache search name_to_search_for
#
# RUN apt-get install -y vim


# If you are on Linux and have problems with permissions then
# you can run on your normal machine:
# id -u  # write down the result and use it as XXX below
# id -g  # write down the result and use it as YYY below
# Then modify and uncomment these lines:
#RUN useradd -r -u XXXX -g YYYY user
#USER user

# Here is an example of installing the Julia language:
# RUN mkdir /opt/julia \
#   && cd /opt/julia \
#   && wget https://julialang-s3.julialang.org/bin/linux/x64/1.0/julia-1.0.3-linux-x86_64.tar.gz \
#   && tar -zxf julia-1.0.3-linux-x86_64.tar.gz \
#   && cp -r julia-1.0.3/* /usr/local/ \
#   && rm -rf /opt/julia

