CosmoSIS-Docker
===============

This tool uses the "Docker" system to set up the libraries that CosmoSIS depends
on inside a "virtual machine"-like system.

It is suitable for desktops and laptops through probably not for clusters and supercomputers without specific effort.  It will work on Linux and OSX.


Instructions
===============
To install CosmoSIS with this tool:

1.  Install docker (Download https://download.docker.com/mac/stable/Docker.dmg on OSX) from www.docker.com
2.  Go to https://hub.docker.com/ and sign up for an account
3.  Start Docker running by following the instructions on the site. You do not
    need to go through the tutorials.
4.  Run `docker login` on the command line and use the username and password you just made for docker hub
5.  Download this tool by running: **git clone https://bitbucket.org/joezuntz/cosmosis-docker**
6.  cd to the new cosmosis-docker directory and run: **./get-cosmosis-and-vm ./cosmosis**
7.  Once the download and installation process are complete, 
    run: **./start-cosmosis-vm ./cosmosis**
8. (temporary) First time only, run update-cosmosis --develop to get the development version which has some fixes in. 
9.  The first time you do this (or to recompile later), run: **make**

In future you just need to do step 5.

Your terminal will now be in the CosmoSIS virtual machine, with a different 
operating system and files than your usual computer. The only overlap is the 
directory /cosmosis where you start, which is mapped to the subdirectory 
cosmosis which will be downloaded next to this readme file.

You should use the "virtual" terminal just to run make, cosmosis, and 
postprocess, and use a separate terminal for everything else like editing 
files and viewing plots.

F.A.Q
=======

Questions below:

- How can I add new libraries, packages, and other dependencies?
- How can I access other directories from my real computer?
- Something has gone wrong when I updated to a new version of cosmosis-docker
- What is this Docker thing?
- Why do I need this? Why can't I just compile my code normally?
- Does this affect code speed?
- Docker isn't a virtual machine - why do you keep calling it one?


How can I add new libraries, packages, and other dependencies?
--------------------------------------------------------------

Any changes you make to the virtual machine (outside the /cosmosis directory) 
are lost when you exit the machine.  If you want to make permanent changes, for
example to load a new library or to install a new tool, then you can do it like 
this:

1.  Edit the Dockerfile in this directory. Add to the end of the file some 
    commands like this:
    RUN installation_commands
    There are some examples in the file.
2.  Run ./rebuild-cosmosis-vm
3.  The next time you run ./start-cosmosis-vm your changes will be present

How can I rebuild the dependency code if it has changed?
--------------------------------------------------------

Run ./full-rebuild-cosmosis-vm



How can I access directories from my real computer?
--------------------------------------------------------------

You can map a directory on your real disc to one in the virtual machine.

If you want to add a directory with path on your machine:
/path/to/my/data
so that inside the virtual machine it is called:
/data
then you would start the VM like this:
./start-cosmosis-vm ./cosmosis --volume /path/to/my/data:/data


Something has gone wrong when I updated to a new version of cosmosis-docker
---------------------------------------------------------------------------

You may need to run the commands:

    docker pull joezuntz/cosmosis
    ./rebuild-cosmosis-vm



What is this Docker thing?
--------------------------

Docker is like a light-weight virtual machine, as if you had a whole second 
computer with its own operating system and files living inside your real 
machine. You can log into this machine from your real computer and use it a bit
like you can log into another remote computer via ssh.

Because this second machine can be any operating system we want, the CosmoSIS 
developers can make sure it is one that we know will work happily with CosmoSIS 
and have all the things the code needs pre-installed.



Why do I need this? Why can't I just compile my code normally?
---------------------------------------------------------------

You absolutely could, but it would be quite hard and take a while.

Because CosmoSIS brings together a lot of other people's software it has many 
different dependencies, and it can take a while to build them all and be fraught
with difficulty.

And helping to debug any problems with installation can be very hard because of
all the different systems people work on. With this Docker system everyone is
effectively on the same machine.


Does this affect code speed?
-------------------------------

The performance effects are very small when you compare the same number of cores
for the same job.  But make sure that's what you are doing: the OMP_NUM_THREADS
environment variable can affect your timing.

Also make sure you tell Docker to use the full RAM and cores that your machine
has, through the Docker Preferences menu.



Docker isn't a virtual machine - why do you keep calling it one?
----------------------------------------------------------------

Docker technically provides "images" and "containers" not virtual machines. But
the latter name is more familiar to most people and from the perspective of an 
end user there isn't that much difference between the two.