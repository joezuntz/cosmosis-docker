#!/usr/bin/env bash

set -e


function show_help {
    echo "get-cosmosis:  Download the CosmoSIS virtual machine and code"
    echo "Syntax: get-cosmosis [-d] [-x]  install_directory "
    echo " -d: Download the cosmosis-des-library repository (requires collaboration password)"
    echo " -x: Download the development version of cosmosis (more features; more bugs; less documentation)"
    echo " -k: Skip downloading cosmosis; assume the install_directory already has a downloaded version"
}

branch=master
download_des=0
skip=0
while getopts "h?xdk" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    x)  branch=develop
        ;;
    d)  download_des=1
        ;;
    k)  skip=1
        ;;
    esac
done
shift $(expr $OPTIND - 1 )

install_dir="$1"
if [ -z ${install_dir} ]
    then
    show_help
    echo "Please specify an installation directory" 1>&2
    echo "You can use the -k flag first if you have already downloaded cosmosis into it" 1>&2
    exit 1
fi


type docker &>/dev/null 2>&1 || \
    { echo "You need to install Docker from https://www.docker.com/ to use cosmosis-docker but I could not find the docker command so it looks like you haven't." ; exit 1; }

docker info &> /dev/null || \
    { echo "Docker is installed, but not actually running - please set it running." ; exit 1; }


if [ ${skip} -ne 1 ] 
    then
    if [ -e ${install_dir} ]
        then
        echo "The directory ${install_dir} already exists. Please pick a new, empty path" 1>&2
        exit 1
    fi

    #Need a directory to install cosmosis
    git clone -b $branch https://bitbucket.org/joezuntz/cosmosis $install_dir
    pushd $install_dir
    git clone -b $branch https://bitbucket.org/joezuntz/cosmosis-standard-library


    if [ ${download_des} -eq 1 ]
        then

        echo
        echo "************************************************************"
        echo "* Cloning the Dark Energy Survey Library: please enter the "
        echo "* collaboration password if you are given a prompt."
        echo "************************************************************"
        echo

        git clone https://darkenergysurvey@bitbucket.org/joezuntz/cosmosis-des-library
    fi

    popd

else
    echo "Using pre-existing cosmosis installation in ${install_dir}"
fi

#Now build our docker image.
./rebuild-cosmosis-vm
