#!/usr/bin/env bash

set -e
USERNAME=$(echo $USER |  tr '[:upper:]' '[:lower:]')
cosmosis_dir="$1"
if [ -z ${cosmosis_dir} ]
    then
    echo "Syntax: start-cosmosis-vm  cosmosis-directory [optional docker commands]" 1>&2
    exit 1
fi

get_abs_filename() {
  # $1 : relative filename
  echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
}

cosmosis_dir=$(get_abs_filename ${cosmosis_dir})

if [ ! -e  ${cosmosis_dir}/bin/cosmosis ]
    then
    echo "The directory you specified does not seem to contain a CosmoSIS installation"
    echo "(${cosmosis_dir})"
    echo
    echo "If you are just starting out and haven't downloaded it yet you can run:"
    echo "./get-cosmosis-and-vm"
    echo
    echo "Or maybe you just typed the wrong directory."
    echo
    exit 1
fi

shift

docker_options=$@

type docker >/dev/null 2>&1 || \
    { echo "scct image requires Docker, which seems not to be installed. Please install Docker." ; exit 1; }

docker info > /dev/null || \
    { echo "scct requires a running Docker daemon." ; exit 1; }

echo 
echo " ------------------------------------------------------------------------- "
echo "|  You are now entering the CosmoSIS Docker Container (virtual machine)  |"
echo "|  Please read these four facts:                                         |"
echo " ------------------------------------------------------------------------- "
echo
echo "1) It is as if you were now running on a different machine with its own"
echo "   operating system, directories, and files."
echo 
echo "2) The virtual machine directory /cosmosis is mapped to your real computer"
echo "   directory: ${cosmosis_dir} "
echo "   Other files (like data directories or external things) are not"
echo "   here.  If you need other directories then see README.txt"
echo 
echo "3) Many of your normal terminal commands may not work here."
echo "   You should open a new terminal to do things like edit files and"
echo "   look at results and just run make, cosmosis, and postprocess here"
echo "   You do not need to source anything before making - we did that for you."
echo
echo "4) Changes you make OUTSIDE this directory will be lost when you exit"
echo "   Put things you want to keep in this dir.  If you need to install"
echo "   new external code for your modules then see README.txt"
echo
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "    Type exit or press ctrl-d to exit."
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo
echo "If this is your first time then you want to run 'make' now"
echo "But first, *seriously*, read the four warnings above!"
echo

docker run --rm --interactive --tty --volume ${cosmosis_dir}:/cosmosis ${docker_options} --workdir /cosmosis ${USERNAME}/cosmosis-vm-py3:latest  /bin/bash

echo 
echo "You have now left the CosmoSIS Docker Container and returned to your normal shell"
echo "We hoped you enjoyed your visit. Come back soon!"
echo 
